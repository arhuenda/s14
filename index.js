
// Create a function which will be able to add two numbers
function displaySum(n1,n2){
	console.log(n1+n2);
};

displaySum(10,20);

// Create a function which will be able to subtract two numbers
function displayDifference(n1,n2){
	console.log(n1-n2);
};

displayDifference(55,40);

// Create function which will be able to multiply two numbers
function displayProduct(n1,n2){
	console.log(n1*n2);
};

displayProduct(12,3);

// Create a new variable called product
function displayProduct2(x,y){
	return x * y;
};


let product = displayProduct2(10,5);
console.log(product);